import { Component } from '@angular/core';
import { NgbCarouselConfig } from '@ng-bootstrap/ng-bootstrap';
@Component({
  selector: 'app-componente2',
  templateUrl: './componente2.component.html',
  styleUrls: ['./componente2.component.css'],
  providers: [NgbCarouselConfig]
})
export class Componente2Component {
  images = [
    {title: '', short: ' ', src: "assets/img/port3.jpg"},
    {title: '', short: '', src: "assets/img/reglamento.png"},
    {title: '', short: '', src: "assets/img/port2.jpg"}
  ];

  constructor(config: NgbCarouselConfig) {
    config.interval = 4000;
    config.keyboard = true;
    config.pauseOnHover = true;
  }
}
